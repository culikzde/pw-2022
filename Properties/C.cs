﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Properties
{
    public enum E
    {
        small,
        medium,
        large
    }

    public class C
    {
        int x;

        // using System.ComponentModel;
        [DisplayName("x-ova souradnice")]
        [Description ("Vlozte x-ovu souradnici")]
        [Category("Souradnice")]
        public int X { get => x; set => x = value; }

        int y;

        [Category("Souradnice")]
        public int Y 
        { 
            get { return y; } 
            set { y = value; } 
        }

        public E Size { set; get; }
        public int Number { set; get; }
        public string Name { set; get; }

        // using System.Drawing;
        public Color Ink { set; get; }
        public Color Paper { set; get; }
        public Font Font { set; get; }

        public string [] Lines { set; get; }

        public D Data { get; set; }

        // using System.Windows.Forms;
        public Button Knoflik { get; set; }
        

        public C ()
        {
            Ink = Color.Red;
            Paper = Color.Lime;
            Lines = new string [] { "abc", "def" };
            Data = new D ();
            Knoflik = new Button();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Properties
{
    public partial class Form1 : Form
    {
        private C config;

        public Form1()
        {
            InitializeComponent();

            Control c = propGrid;
            while (c.Parent != null)
                c = c.Parent;

            display (treeView.Nodes, c);

            // propGrid.SelectedObject = c;

            config = new C();
            propGrid.SelectedObject = config;
        }

        private void display (TreeNodeCollection target, Control c)
        {
            TreeNode node = new TreeNode();
            node.Text = c.Name + ":" + c.GetType().Name;
            node.Tag = c;
            target.Add(node);
            
            foreach (Control u in c.Controls)
            {
               display (node.Nodes, u);
            }
        }

        object selected_object = null;

        private void treeView_NodeMouseClick (object sender, TreeNodeMouseClickEventArgs e)
        {
            propGrid.SelectedObject = e.Node.Tag;

            listView.Items.Clear();

            object obj = e.Node.Tag;
            Type cls = obj.GetType();
            selected_object = obj;


            // using System.Reflection;
            try
            {
                PropertyInfo[] prop_list = cls.GetProperties();
                foreach (PropertyInfo prop in prop_list)
                {
                    ListViewItem node = new ListViewItem();
                    node.Text = prop.Name;
                    node.SubItems.Add (prop.PropertyType.Name);
                    object value = null;
                    if (prop.CanRead)
                    {
                        object [] param_list = { };
                        value = prop.GetMethod.Invoke (obj, param_list);
                    }
                    node.SubItems.Add(""+value);
                    node.Tag = prop;
                    listView.Items.Add(node);
                }
            }
            catch (Exception exc)
            {

            }
        }

        private void listView_ItemActivate(object sender, EventArgs e)
        {
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            string text = textBox.Text;
            if (listView.SelectedItems.Count != 0)
            {
                PropertyInfo prop = listView.SelectedItems[0].Tag as PropertyInfo;
                if (selected_object != null && prop != null)
                {
                    object value = text;
                    if (prop.PropertyType == typeof (int))
                    {
                        int n = int.Parse(text);
                        value = n;
                    }
                    object[] param_list = { value };
                    prop.SetMethod.Invoke (selected_object, param_list);
                }
            }
        }
    }
}

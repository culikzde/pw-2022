﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Table
{
    public partial class Window : Form
    {
        public Window()
        {
            InitializeComponent();

            grid.ColumnCount = 10;
            for (int i = 1; i <= 10; i++)
            {
                object [] data = new object [10];
                for (int k = 1; k <= 10; k++)
                {
                    data[k - 1] = i + k;
                }
                grid.Rows.Add(data);
            }

            for (int i = 1; i <= 10; i++)
            {
                for (int k = 1; k <= 10; k++)
                {
                    DataGridViewCell cell = grid.Rows[i - 1].Cells[k - 1];
                    cell.ToolTipText = "(" + i + "," + k + ")";
                    if (i == k)
                    {
                        cell.Style.BackColor = Color.LightYellow;
                        cell.Style.ForeColor = Color.Red;
                    }
                    else
                    {
                        cell.Style.ForeColor = Color.Blue;
                    }
                }
            }
        }

        private void runMenuItem_Click(object sender, EventArgs e)
        {
            const int lines = 10;
            const int columns = 10;
            double [,] data = new double [lines, columns];
            for (int i = 0; i < lines; i++)
                for (int k = 0; k < columns; k++)
                {
                    var cell = grid.Rows[i].Cells[k];
                    string s = cell.Value.ToString();
                    s = s.Replace(" ", "");
                    double x;
                    bool ok = double.TryParse (s, out x);
                    if (ok)
                    {
                        data[i, k] = x;
                    }
                }

            info.Clear();
            for (int i = 0; i < data.GetLength (0); i++)
            {
                string txt = "[ ";
                for (int k = 0; k < data.GetLength (1); k++)
                {
                    txt = txt + data[i, k];
                    if (k < data.GetLength(1) -1 ) txt = txt + ", ";
                }
                txt = txt + " ]";
                info.AppendText(txt + "\r\n");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw
{
    public partial class DrawForm : Form
    {
        const int line = 0;
        const int rectangle = 1;
        const int ellipse = 2;

        private int button_count = 1;

        public Pen pen;
        public Brush brush;
        public Color brushColor1;
        public Color brushColor2;

        public DrawForm()
        {
            InitializeComponent();
            pictureBox_SizeChanged (null, null);

            Panel first = colorPanel;

            Panel second = newButton (Color.Blue);
            Panel third = newButton(Color.Green);

            pen = new Pen (first.BackColor, 3);

            brushColor1 = second.BackColor;
            brushColor2 = second.BackColor;
            gradientBrush();

            comboBox.SelectedIndex = line;
        }

        // public Color getColor1() { return brushColor1; }
        // public Color getColor2() { return brushColor2; }
        // public void setColor1(Color c) { brushColor1 = c; }
        // public void setColor2(Color c) { brushColor2 = c; }

        private Panel newButton (Color color)
        {
            Panel p = new Panel ();
            p.BackColor = color;
            p.Left = colorPanel.Left + button_count * (colorPanel.Width + colorPanel.Left);
            p.Top = colorPanel.Top;
            p.Width = colorPanel.Width;
            p.Height = colorPanel.Height;
            p.MouseDown += colorPanel_MouseDown;
            button_count++;
            p.Parent = toolPanel;
            return p;
        }

        public void gradientBrush()
        {
            // using System.Drawing.Drawing2D;
            int x = (int) numX.Value;
            int y = (int) numY.Value;
            brush = new LinearGradientBrush (new PointF(0, 0),
                                             new PointF(x, y),
                                             brushColor1,
                                             brushColor2);
        }

        private void colorPanel_MouseDown (object sender, MouseEventArgs e)
        {
            // Panel p = (Panel) sender ;
            Panel p = sender as Panel;

            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Shift)
            {
                brushColor1 = p.BackColor;
                gradientBrush ();
            }
            else if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Control)
            {
                brushColor2 = p.BackColor;
                gradientBrush ();

            }
            else if (e.Button == MouseButtons.Left)
            {
                brushColor1 = p.BackColor;
                brushColor2 = p.BackColor;
                gradientBrush();
            }
            else if (e.Button == MouseButtons.Right)
            {
                pen.Color = p.BackColor;
            }
            else if (e.Button == MouseButtons.Middle)
            {
                colorDialog.Color = p.BackColor;
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    p.BackColor = colorDialog.Color;
                    brush = new SolidBrush(p.BackColor);
                    brushColor1 = p.BackColor;
                    brushColor2 = p.BackColor;
                    gradientBrush();
                }
            }
        }

        private void newColorMenuItem_Click(object sender, EventArgs e)
        {
            newButton (Color.Yellow);
        }

        private void pictureBox_SizeChanged (object sender, EventArgs e)
        {
            Image orig = pictureBox.Image;

            int w = pictureBox.Width;
            int h = pictureBox.Height;

            if (orig != null)
            {
                if (orig.Width > w) w = orig.Width;
                if (orig.Height > h) h = orig.Height;
            }

            Bitmap bitmap = new Bitmap (w, h);
            Graphics g = Graphics.FromImage (bitmap);
            Brush b = new SolidBrush(Color.White);
            g.FillRectangle(b, 0, 0, w, h);

            if (orig != null)
               g.DrawImage (orig, 0, 0);

            pictureBox.Image = bitmap;
        }

        private int X0, Y0;
        private bool down = false;
        private Image save;

        private void pictureBox_MouseDown (object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            down = true;
            save = new Bitmap (pictureBox.Image);
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                Graphics g = Graphics.FromImage (pictureBox.Image);
                g.DrawImage (save, 0, 0);

                int tool = comboBox.SelectedIndex;

                if (tool == line)
                {
                    g.DrawLine (pen, X0, Y0, e.X, e.Y);
                }
                else if (tool == ellipse)
                {
                    g.FillEllipse (brush, X0, Y0, e.X - X0, e.Y - Y0);
                    g.DrawEllipse (pen, X0, Y0, e.X - X0, e.Y - Y0);
                }
                else if (tool == rectangle)
                {
                    int X1 = X0;
                    int Y1 = Y0;
                    int X2 = e.X;
                    int Y2 = e.Y;
                    if (X2 < X1) { int T = X1; X1 = X2; X2 = T; }
                    if (Y2 < Y1) { int T = Y1; Y1 = Y2; Y2 = T; }
                    g.FillRectangle(brush, X1, Y1, X2 - X1, Y2 - Y1);
                    g.DrawRectangle(pen, X1, Y1, X2 - X1, Y2 - Y1);
                }

                pictureBox.Invalidate ();
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
        }

        private void openMenuItem_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = openDialog.FileName;
                pictureBox.Image = new Bitmap (fileName);
                // pictureBox.Invalidate();
                pictureBox_SizeChanged (null, null);
            }
        }

        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveDialog.FileName;
                pictureBox.Image.Save (fileName);
            }
        }

        private void numX_ValueChanged(object sender, EventArgs e)
        {
            gradientBrush();
        }

        private void editPenMenuItem_Click(object sender, EventArgs e)
        {
            PenDialog dlg = new PenDialog (pen);
            dlg.Show ();
        }

        private void editBrushMenuItem_Click(object sender, EventArgs e)
        {
            BrushDialog dlg = new BrushDialog (this);
            dlg.Show();
        }

        private void quitMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}

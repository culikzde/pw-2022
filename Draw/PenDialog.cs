﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw
{
    public partial class PenDialog : Form
    {
        public PenDialog (Pen pen)
        {
            InitializeComponent();
            propertyGrid.SelectedObject = pen;
        }
    }
}

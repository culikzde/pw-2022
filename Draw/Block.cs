﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw
{
    public partial class Block : UserControl
    {
        public Block()
        {
            InitializeComponent();
        }

        private int X0, Y0;
        private bool down = false;

        private void Block_MouseDown(object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            down = true;
        }

        private void Block_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                if (e.Button == MouseButtons.Right)
                {
                    this.Width += e.X - X0;
                    this.Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                }
                else
                {
                    this.Left += e.X - X0;
                    this.Top += e.Y - Y0;
                }
            }
        }

        private void openMenuItem_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = openDialog.FileName;
                this.BackgroundImage = new Bitmap (fileName);
            }

        }

        private void Block_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;

        }
    }
}

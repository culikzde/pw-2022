﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Windows.Forms;

namespace Draw
{
    class BrushManipulator
    {
        private DrawForm win;

        public BrushManipulator (DrawForm win0)
        {
            win = win0;
        }

        public Color firstColor
        {
            get { return win.brushColor1; }
            set { win.brushColor1 = value; win.gradientBrush(); }

            // Color get () { return brushColor1; }
            // void set (Color value) { brushColor1 = value; }
        }

        public Color secondColor
        {
            get => win.brushColor2;
            set { win.brushColor2  = value; win.gradientBrush(); }
        }

        public int X
        {
            get { return (int)win.numX.Value; }
            set { win.numX.Value = value; }
        }

        public int Y
        {
            get { return (int)win.numY.Value; }
            set { win.numY.Value = value; }
        }
    }
}

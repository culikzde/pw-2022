﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw
{
    // public delegate void Refresh();

    // public delegate Color GetColor();
    // public delegate void SetColor(Color c);

    public partial class BrushDialog : Form
    {
        private BrushManipulator manip;

        public BrushDialog (DrawForm win0)
        {
            InitializeComponent();
            manip = new BrushManipulator (win0);
            propertyGrid.SelectedObject = manip;

        }

        private void propertyGrid_Click(object sender, EventArgs e)
        {

        }
    }
}
